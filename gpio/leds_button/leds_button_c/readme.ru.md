�������� ����� �� STM32F4 - GPIO {#mainpage}
==========================================
������ � 1. ��������� � ������ ����� GPIO
==========================================
������� ������������ � ��������� ������� ������
-----------------------------------------------
@author  ������ http://cybergnosis.su
@project [STM32:������](http://cybergnosis.su/category/stm32/stm32base/)
@repository https://bitbucket.org/cybergnosis/stm32_base/
@hardware
 * [STM32F4DISCOVERY](http://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-eval-tools/stm32-mcu-eval-tools/stm32-mcu-discovery-kits/stm32f4discovery.html)
 * [STM32F407VGT6](http://www.st.com/en/microcontrollers/stm32f407vg.html)
 @software
 * [CoIDE](http://www.coocox.org/software/coide.php)
 * [GNU ARM Embedded Toolchain](https://launchpad.net/gcc-arm-embedded/)
 * [STM32 ST-LINK utility](http://www.st.com/content/st_com/en/products/embedded-software/development-tool-software/stsw-link004.html)
 @article
 * [GPIO:������](http://cybergnosis.su/stm32base-gpio-theory/)
 * [GPIO:�������� � ������� ������������ � ��������� ������� ������ (C)](http://cybergnosis.su/stm32base-gpio-leds-button-c/)
 
������� ��������
--------------------
 ���������� ���������/���������� ������������� �� ����� ����������������
 ����������� (�� ������� ��� ������ ������� �������) � ���������
 ����������� ������������ �� ��������������� ��� �������
 �� ���������������� ������.
 
��������
----------
@image html algorithm.jpg
@warning  ������ ����������� ����������� ����������� �
 ����� ������������. �������� ����� ������������ ���������� �
 ���� ����� ��� �� ������������ ����������, ��� ���� ����� ���������,
 ��� ����� ����� ��� ������� �������������� �� ����� ��������� �����������,
 ��������� � �������������� ��� ���������������� ������� ����, �� ����.
 ����������, �����������, ������� � �������� �� ���������� ������
 ����������� �����������, �� ������� ��� �� ���� ����� � ����!

�������� ���
-------------

@includelineno main.c

