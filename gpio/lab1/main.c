#include "stm32f4xx.h" //CMSIS Cortex-M4 ������������ ���� ������ �������
                       //� ������������ �����������
					   //���� ���� �������� ����������� ���� ��������� ���������, ����������� ���
					   //� ����� ������ ��� STM32F4xx ���������
#include "stm32f4xx_rcc.h" //���� ���� ������������� ������� ��� ���������� ����������
                           //������������������:
                           //- ����������������� ������ � �������� �������� �������
                           //(Reset and clock control - RCC) ���������,
                           //- ����������/������� ���������� ������������, ������������,
                           //������� �������������� ������� (phase-locked loop - PLL),
                           //������� ������������ ������������ (Clock security system - CSS),
                           //������ ������� ���������������� (Master Clock Output  - MCO)
                           //- ������������� ������������ ��������� ���� � ��� AHB � APB
                           //- ������������� ������������ ���������
                           //- ������������ � ������������ �������
#include "stm32f4xx_gpio.h" //���� ���� ������������ ������� ��� ���������� ����������
						    //������������������ ������ �����/������ ������ ����������
                            //(general-purpose I/O port - GPIO):
						    //- �������������� � �������������
						    //- ������� � ������� ������ �����/������
						    //- ��������� �������������� ������������ ������ �����/������

//������������ ������� ����������� ��������
//�������������� � ����� ���������� ��������
//� ���� �������  ����� ��� ����������� ������� ������� ����������
//� ��� ������������ �������������� �������� ��������� ������
void Delay(uint32_t nCount){
	while(nCount--){
	}
}

int main(void)
{   //���������� ����������
	//������ ������������ �����
	uint16_t ledPins[] = {GPIO_Pin_12, GPIO_Pin_13, GPIO_Pin_14, GPIO_Pin_15};
	//������ � ������� ������������ �����, ������������, ����� ��������� ������ ������
	uint8_t ledPinIndex = 0;
	//����������� ������������ �����������, 0 - �� ������� �������, 1 - ������
	uint8_t ledDirection = 0;
	GPIO_InitTypeDef gpioConf; //��������� ������������� ����� �����-������

	//������ ������������ ����
	//��������� ������������ ����� � � D
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOD, ENABLE);
	//������������� �����, ������������� � ������
		//�������������� ���������
	gpioConf.GPIO_Pin = GPIO_Pin_0; //��� ����� � �������� ���������� ���������������� ������
	gpioConf.GPIO_Mode = GPIO_Mode_IN; //��� �� ����
	GPIO_Init(GPIOA, &gpioConf); //��������������

	//������������� �������, ������������ � �����������
		//�������������� ���������
	gpioConf.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;  //���� �����������
	gpioConf.GPIO_Mode = GPIO_Mode_OUT; //���� �� �����
	gpioConf.GPIO_Speed = GPIO_Speed_2MHz; //�������� ��������� ����������,
											 //������������� �������� ������� = speed/2
	gpioConf.GPIO_OType = GPIO_OType_PP; //��� ������ - ���������� ������
	gpioConf.GPIO_PuPd = GPIO_PuPd_NOPULL; //��� ��������
	GPIO_Init(GPIOD, &gpioConf); //��������������

	//����������� ���� ������������ ����������� � ������������ ������� �� ������
    while(1)
    {
    	//�������� ������� �� ������ � ��������� ����������� ������������ �����������
    	if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)) //��� �������� � ����
    	{										     //��� ������� �� ������ �� ��� ���������� 1
    												 //����� ��� ���������� - ����������� � ����
    												 //����� �������
    		Delay(5000); //��������, ��� �������������� �������� ���������
    		while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)); //�������� � ����� ����
    														 //������ ������
    		//�������� ����������� ������������ ����������� �� ���������������
    		ledDirection ^= 1;
    	}
    	//������� ������������
    	GPIO_SetBits(GPIOD, ledPins[ledPinIndex]); //�������� ���������, ������� ������������ � �������
    	                                           //ledPins ��������� ledPinIndex
    	Delay(500000);	//���� ������ �������� - ��������� ���������� ������
    	GPIO_ResetBits(GPIOD, ledPins[ledPinIndex]); //����� ���������
    	//���������� ���������, ������� ����� ����� �� ����� �������� �����
    	if(ledDirection){  //� ����������� �� ����������� ������������
    		ledPinIndex--; //���������
    	} else {
    		ledPinIndex++; //��� ����������� ������
    	}
    	if(3 < ledPinIndex){ //���� ������ ���� ������ 3 (4 ��� ����������, 255 ��� ����������)
    		ledPinIndex = (ledDirection) ? 3 : 0; //� ����������� �� ����������� ������������� ���
    	}                                         //� 3 ��� � 0
    }
}
